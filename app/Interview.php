<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Interview extends Model
{
    public function candidates(){
        return $this->hasMant('App\Candidate');
    }
    public function users(){
        return $this->hasMant('App\User');
    }
}
