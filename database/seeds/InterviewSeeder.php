<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class InterviewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('interviews')->insert([
            'description' => Str::random(100),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);  
    }
}
