@extends('layouts.app')

@section('title', 'Interviews')

@section('content')

            <div class="content">
                <div class="title m-b-md">
                    <div><a href =  "{{url('/interviews/create')}}"> Add new interview</a></div>
                    <h1>List of interviews</h1>
                    <table class = "table table-dark">
                        <tr>
                            <th>Description</th><th>Candidate</th><th>User</th><th>Created</th><th>Updated</th>
                        </tr>
                        @foreach($interviews as $interview)
                        <tr>
                            <td>{{$interview->description}}</td>
                            <td>
                                <div class="dropdown">
                                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Dropdown button
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-menu" href="#">Action</a>
                                    </div> 
                                    <td>
                                        <div class="dropdown">
                                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Dropdown button
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <a class="dropdown-menu" href="#">Action</a>
                                            </div> 
                                        </td>
                                </td>
                                    </tr>  
                                    
                        @endforeach
                    </table>
                </div>
            </div>
    </body>
    @endsection
